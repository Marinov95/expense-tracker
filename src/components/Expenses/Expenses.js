import "./Expenses.css";
import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesChart from "./ExpensesChart";
import React, { useState } from "react";

const Expenses = (props) => {
  const { expenses } = props;
  const [filteredDate, setFilteredDate] = useState("2022");
  const saveFilteredExpense = (selectedDate) => {
    setFilteredDate(selectedDate);
  };
  const filteredExpenses = expenses.filter(
    (el) => el.date.getFullYear().toString() === filteredDate
  );

  let expensesContent = <p>No expenses found for the current year</p>;
  if (filteredExpenses.length > 0) {
    expensesContent = filteredExpenses.map((el) => {
      return (
        <ExpenseItem
          key={el.id}
          title={el.title}
          amount={el.amount}
          date={el.date}
        />
      );
    });
  }
  return (
    <Card className="expenses">
      <ExpensesChart expenses={filteredExpenses} />
      <ExpensesFilter
        selected={filteredDate}
        onFilterExpense={saveFilteredExpense}
      />
      {expensesContent}
    </Card>
  );
};

export default Expenses;

import React, { useState } from "react";
import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";

const NewExpense = (props) => {
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
  };
  const [showForm, setShowForm] = useState(false);
  const showFormHandler = () => {
    setShowForm(!showForm);
  };

  return (
    <div className="new-expense">
      {showForm ? (
        <ExpenseForm
          closeForm={showFormHandler}
          onSaveExpenseData={saveExpenseDataHandler}
        />
      ) : (
        <form>
          <button type="button" onClick={showFormHandler}>Add New Expense</button>
        </form>
      )}
    </div>
  );
};
export default NewExpense;
